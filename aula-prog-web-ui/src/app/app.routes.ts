import { Routes, RouterModule } from '@angular/router';

import { ProdutoComponent } from './produto/produto.component';
import { CategoriaComponent } from './categoria/categoria.component';
import { CategoriaFormularioComponent } from './categoria-formulario/categoria-formulario.component';
import { ProdutoFormularioComponent } from './produto-formulario/produto-formulario.component';

export const routes : Routes =[
    { path: '', redirectTo: '', pathMatch: 'full' },
    {path: 'produto', component: ProdutoComponent},
    {path: 'categoria', component: CategoriaComponent},
    {path: 'form-categoria', component: CategoriaFormularioComponent},
    {path: 'form-produto', component: ProdutoFormularioComponent}
    
];

export const appRoutingProviders: any[] = [];
    
export const routing = RouterModule.forRoot(routes);