import { Injectable } from '@angular/core';

import {Http, Headers, HttpModule} from '@angular/http';
import 'rxjs/add/operator/map';

import {Categoria} from './categoria';

@Injectable()
export class CategoriaService {

    public API_URL: string = 'http://localhost:8080';
    
    constructor(public http : Http){}

    getCategorias(): any{
        return new Promise((resolve, reject) => {
            this.http
                    .get(`${this.API_URL}/categoria`)
                    .map(res => res.json())
                    .subscribe(
                        data => {
                            resolve(data.content)
                        },
                        error => {
                            reject(error);
                        }
                    )
        });
    }

    postCategoria(data): any{
        console.log(data);
        return {}; 
        // return new Promise((resolve, reject) => {
        //     this.http
        //         .post((`${this.API_URL}/categoria`), data)
        //         .map(res => res.json())
        //         .subscribe(
        //             data => {
        //                 resolve(data.content)
        //             },
        //             error => {
        //                 reject(error);
        //             }
        //         )
        // });
    }

    deleteCategorias(id: number): any{
        return new Promise((resolve, reject) => {
            this.http
                    .delete(`${this.API_URL}/categoria/${id}`)
                    .map(res => res.json())
                    .subscribe(
                        data => {
                            resolve(data.content)
                        },
                        error => {
                            reject(error);
                        }
                    )
        });
    }
    

}