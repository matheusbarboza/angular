import { Component, OnInit } from '@angular/core';

import {CategoriaService} from './categoria.service';
import {Categoria} from './categoria';

@Component({
  selector: 'app-categoria',
  templateUrl: './categoria.component.html',
  styleUrls: ['./categoria.component.css']
})
export class CategoriaComponent implements OnInit {

  categorias : Categoria[];


  constructor(private categoriaService : CategoriaService) {

    this.categoriaService.getCategorias()
      .then(result => {
        console.log(result)   
        this.categorias = result;
      })
      .catch( error => {
        console.log(' ----- ERROR ----- '+ error.message);
      });

      

   }

  ngOnInit() {
  }

  public deletarCategoria(id: number) {
    this.categoriaService.deleteCategorias(id)
      .then(result => {
        this.categorias = result;
      })
      .catch(error => {
        console.log(' ----- ERROR ----- '+ error.message)
      });
  }

}
