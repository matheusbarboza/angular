package com.millys.web.domain.produto;

import com.millys.web.core.controller.RestAbstractController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
@CrossOrigin(origins = "*")
@RestController
@RequestMapping(value = "/produto")
public class ProdutoController extends RestAbstractController {

    @Autowired
    ProdutoService produtoService;

    @GetMapping
    public ResponseEntity<?> buscarTodos(){
        return jsonResponse(produtoService.buscarTodos());

    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<?> buscarPeloId(@PathVariable long id){
        return jsonResponse(produtoService.buscarPorId(id));
    }

    @PostMapping
    public ResponseEntity<?> salvar(@RequestBody Produto produto){
        produtoService.salvar(produto);
        jsonResponseService.addSuccess(ProdutoMessageCode.PRODUTO_SUCESSO_SALVAR);
        return jsonResponse();
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<?> atualizar(@RequestBody Produto produto){
        produtoService.salvar(produto);
        return jsonResponse();
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<?> deletar(@PathVariable long id) {

        try {
            produtoService.deletar(id);
            return new ResponseEntity<Object>(null, HttpStatus.OK);
        } catch (IllegalArgumentException e) {
            return new ResponseEntity<Object>(null, HttpStatus.OK);
        }

    }

}
