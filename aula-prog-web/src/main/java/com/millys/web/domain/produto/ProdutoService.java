package com.millys.web.domain.produto;

import com.millys.web.core.exceptions.DuplicatedException;
import com.millys.web.core.exceptions.ExceptionMessageCode;
import com.millys.web.core.exceptions.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProdutoService {

    @Autowired
    private ProdutoRepository produtoRepository;

    public void salvar(Produto produto){

        if(produto.getId() != 0){

            Produto p = produtoRepository.findOne(produto.getId());
            if(p != null){
                produtoRepository.save(produto);
            } else {
                // lançar exception
            }

        } else {

            List<Produto> produtos = produtoRepository.findByNome(produto.getNome());

            if(produtos.size() > 0){

                System.out.println("---------------- EXCEPTION ");

                throw new DuplicatedException(ExceptionMessageCode.MENSAGEM_DUPLICATED_ERROR);
            } else {

                System.out.println("---------------- SALVAR ");
                produtoRepository.save(produto);
            }

        }


    }

    public void deletar(long id){
        Produto produto = produtoRepository.findOne(id);
        if(produto != null){
            produtoRepository.delete(id);
        } else {
            // Lançar exception
        }
    }

    public Produto buscarPorId(long id){

        Produto p = produtoRepository.findOne(id);

        if(p == null ){
            throw new NotFoundException(ExceptionMessageCode.MENSAGEM_NOT_FOUND);
        } else {
            return produtoRepository.findOne(id);
        }

    }

    public List<Produto> buscarTodos(){
        return produtoRepository.findAll();


    }


}
