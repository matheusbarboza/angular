package com.millys.web.domain.categoria;

import com.millys.web.domain.produto.Produto;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "categoria")
@JsonIgnoreProperties({"produtos"})
public class Categoria implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue( strategy = GenerationType.SEQUENCE, generator = "categoria_id_seq")
    @SequenceGenerator( name = "categoria_id_seq", sequenceName = "categoria_id_seq",
            allocationSize = 1)
    @Column(name = "id")
    @Getter
    @Setter
    private long id;


    @Column(name = "nome")
    @Getter
    @Setter
    private String nome;


    @OneToMany(mappedBy = "categoria")
    @Getter
    @Setter
    private List<Produto> produtos;

}
