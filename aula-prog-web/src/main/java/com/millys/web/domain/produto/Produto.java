package com.millys.web.domain.produto;

import com.millys.web.domain.categoria.Categoria;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "produto")
public class Produto implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue( strategy = GenerationType.SEQUENCE, generator = "produto_id_seq")
    @SequenceGenerator( name = "produto_id_seq", sequenceName = "produto_id_seq",
            allocationSize = 1)
    @Column(name = "id")
    @Getter
    @Setter
    private long id;


    @Column(name = "nome")
    @Getter
    @Setter
    private String nome;

    @Column(name = "descricao")
    @Getter
    @Setter
    private String descricao;

    @Column(name = "preco")
    @Getter
    @Setter
    private float preco;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "categoria_id")
    @Getter
    @Setter
    private Categoria categoria;

}
