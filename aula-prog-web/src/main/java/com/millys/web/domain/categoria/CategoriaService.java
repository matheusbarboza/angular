package com.millys.web.domain.categoria;

import com.millys.web.core.exceptions.DuplicatedException;
import com.millys.web.core.exceptions.NotFoundException;
import com.millys.web.domain.categoria.Categoria;
import com.millys.web.domain.categoria.CategoriaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoriaService {

    @Autowired
    private CategoriaRepository categoriaRepository;

    public void salvar(Categoria categoria) {
         if(categoria.getId() != 0){
               Categoria c = categoriaRepository.findOne(categoria.getId());
            if(c != null){
                categoriaRepository.save(categoria);
            } else {
                 throw new NotFoundException("Nenhum item encontrado");
            }
         }else{
              List<Categoria> ca = categoriaRepository.findByNome(categoria.getNome());

            if(ca.size() > 0){

                System.out.println("---------------- EXCEPTION ");

                throw new DuplicatedException("Item já existe");
            } else {

                System.out.println("---------------- SALVAR ");
                categoriaRepository.save(categoria);
            }

         }
        
        categoriaRepository.save(categoria);
    }

    public void deletar(long id) {
        Categoria categoria = categoriaRepository.findOne(id);
        if (categoria != null) {
            categoriaRepository.delete(id);
        } else {
             throw new NotFoundException("Nenhum item encontrado");
        }
    }

    public Categoria buscarPorId(long id) {
         Categoria c = categoriaRepository.findOne(id);

        if(c == null ){
            throw new NotFoundException("Nenhum item encontrado");
        } else {
            return categoriaRepository.findOne(id);
        }

    }

    public List<Categoria> buscarTodos() {
       List<Categoria> ca = categoriaRepository.findAll();
        if(ca != null){
                return ca;
            } else {
                 throw new NotFoundException("Nenhum item encontrado");
            }

    }

    public void update(long id, Categoria categoria) {
         Categoria categoriaBanco = categoriaRepository.findOne(id);
        if (categoriaBanco != null) {
            categoriaBanco.setNome(categoria.getNome());
            categoriaRepository.save(categoriaBanco);
            
        } else {
            throw new NotFoundException("Categoria não existe");
        }
    }

}
