package com.millys.web.domain.categoria;

import com.millys.web.core.controller.RestAbstractController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
@CrossOrigin(origins="*")
@RestController
@RequestMapping(value = "/categoria")
public class CategoriaController extends RestAbstractController {

    @Autowired
    CategoriaService categoriaService;

    @GetMapping
    public ResponseEntity<?> buscarTodos() {
        return jsonResponse(categoriaService.buscarTodos());
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<?> buscarPeloId(@PathVariable long id) {
        return new ResponseEntity<Object>(categoriaService.buscarPorId(id), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<?> salvar(@RequestBody Categoria categoria) {
        categoriaService.salvar(categoria);
        return new ResponseEntity<Object>(null, HttpStatus.CREATED);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<?> deletar(@PathVariable long id) {

        try {
            categoriaService.deletar(id);
            return new ResponseEntity<Object>(null, HttpStatus.OK);
        } catch (IllegalArgumentException e) {
            return new ResponseEntity<Object>(null, HttpStatus.OK);
        }

    }
    @PutMapping(value = "/{id}")
    public ResponseEntity<?> update(@PathVariable long id, @RequestBody Categoria categoria) {
         try {
            categoriaService.update(id, categoria);
            return new ResponseEntity<Object>(null, HttpStatus.OK);
        } catch (IllegalArgumentException e) {
            return new ResponseEntity<Object>(null, HttpStatus.OK);
        }
    }


}
