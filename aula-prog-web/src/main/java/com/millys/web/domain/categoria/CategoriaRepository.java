package com.millys.web.domain.categoria;

import com.millys.web.domain.categoria.Categoria;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoriaRepository extends JpaRepository<Categoria, Long> {
    
    public List<Categoria> findByNome(String nome);
}
