package com.millys.web.core.controller;

public enum JsonResponseType {
    SUCCESS, WARNING, ERROR
}
